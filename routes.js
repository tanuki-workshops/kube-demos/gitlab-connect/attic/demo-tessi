

async function routes (fastify, options) {
  
  fastify.log.info(`🤖 options routes:`)
  fastify.log.info(options)

  console.log("hello")

  let temperatureComponent = options.temperatureComponent
  let humidityComponent = options.humidityComponent
  
  fastify.get(`/hello`, async (request, reply) => {
    return {
		message: `👋 Hello world 🌍🎃`,
        pod: options.podName,
        environment: options.environment
	}
  })

  fastify.get(`/metrics`, async (request, reply) => {
    reply.type("text/plain;charset=UTF-8")

    let content = [
      `# HELP ${temperatureComponent.name}: ${temperatureComponent.help}`,
      `# TYPE ${temperatureComponent.name} counter`,
      `${temperatureComponent.name} ${temperatureComponent.value}`,
      `# HELP ${humidityComponent.name}: ${humidityComponent.help}`,
      `# TYPE ${humidityComponent.name} counter`,
      `${humidityComponent.name} ${humidityComponent.value}`
    ]

    return content.join("\n")
  })


    
}

module.exports = routes
